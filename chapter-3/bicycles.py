# This program creates a list of bicycles and stores them into a variable
bicycles = ['trek', 'cannondale', 'redline', 'specialized']
print(bicycles)


# Now print only one item from the list
print(bicycles[0])


# Now print another item from the list with title case
print(bicycles[2].title())


# Print the last item in the list
print(bicycles[-1])

# Print a message while embedding an item from the list
message = f"My first bicycle was a {bicycles[0].title()}."
print(message)

