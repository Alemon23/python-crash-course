# The sort() method changes the order of a list permanently.
# The sorted() function maintains the original order of a list.

# Create a list of cars
cars = ['bmw', 'audi', 'toyota', 'subaru']
print(cars)

# Sort the list of cars using the sort METHOD
cars.sort()
print("\nThe cars are now sorted in alphabetical order.")
print(cars)

# Sort the list in reverse alphabetical order
cars.sort(reverse=True)
print("\nThe cars are now sorted in reverse.")
print(cars)
print()
print()


# Create a list of cars
cars = ['bmw', 'audi', 'toyota', 'subaru']
print(cars)

# Sort the list using the sorted() FUNCTION
print("\nHere is the list in alphabetical order: ")
print(sorted(cars))

print("\nHere is the original list again: ")
print(cars)
print()

# Use the reverse METHOD
print("Here is the original list in reverse:")
cars.reverse()
print(cars)
print()


number_of_cars = len(cars)
print(f"\nThere are {number_of_cars} cars in the list")
