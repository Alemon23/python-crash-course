# List of motorcycles I have owned in
# chronological order

motorcycles = ['indian', 'kawasai', 'suzuki']

last_owned = motorcycles.pop(2)
first_owned = motorcycles.pop(0)

print(f"The first motorcycle I've owned was a(n) {first_owned.title()}")
print(f"The last motorcycle I've owned was a {last_owned.title()}")
