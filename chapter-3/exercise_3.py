# Make a list that includes at least three people you’d like to invite to dinner. 
# Then use your list to print a message to each person, inviting them to dinner.


# Exercise 3.4
guest_invites = ['alexis', 'jennifer', 'penny']
print(f"Hello {guest_invites[0].title()}, please come to my party.")


# Exercise 3.5
print(f"\nIt turns out that {guest_invites[2].title()} cannot make it to the party.")
guest_invites[2] = 'adalyne'

print("These people are now invited to the party: ", guest_invites)


# Exercise 3.6
print("\nI found a bigger table to invite three more people!")
guest_invites.insert(0, 'sara')
guest_invites.insert(2, 'sam')
guest_invites.append('lilith')

print("Here is a new list of invitees", guest_invites)


# Exercise 3.7
print("\nThe new table won't be coming in on time, I can only invite 2 people.")

guest_one = guest_invites.pop(1)
print(f"Sorry {guest_one}, you won't be able to come.")

guest_two = guest_invites.pop(3)
print(f"Sorry {guest_two}, you won't be able to come.")

guest_three = guest_invites.pop(0)
print(f"Sorry {guest_three}, you won't be able to come.")

guest_four = guest_invites.pop(2)
print(f"Sorry {guest_four}, you won't be able to come.")

print("These people are still invited: ", guest_invites)

del guest_invites[0]
del guest_invites[0]
print(guest_invites)
