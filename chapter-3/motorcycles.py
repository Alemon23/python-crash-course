motorcycles = ['honda', 'yamaha', 'suzuki']
print(motorcycles)

# Change the first item in the list
motorcycles[0] = 'ducati'
print(motorcycles)

# Append another motorcycle to the list
motorcycles.append('honda')
print(motorcycles)


# Empty the list
motorcycles = []

# Append items to the empty list
motorcycles.append('Honda')
motorcycles.append('Yamaha')
motorcycles.append('Ducati')
motorcycles.append('Suzuki')

print(motorcycles)

# Insert new item at a specific index
motorcycles.insert(3,'Kawasaki')

print(motorcycles)


# Delete an item from the list using index position
del motorcycles[1]

print(motorcycles)

# Use pop method to remove last item from the list
motorcycles = ['honda', 'yamaha', 'kawasaki']
print(motorcycles)

popped_motorcycles = motorcycles.pop()
print(motorcycles)
print(popped_motorcycles)
print()

# Remove an item by it's value instead of index
motorcycles = ['honda', 'yamaha', 'kawasaki']
print(motorcycles)
too_expensive = 'yamaha'
motorcycles.remove(too_expensive)

print(f"\nA {too_expensive} is too expensive for me")
