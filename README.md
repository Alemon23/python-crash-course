# Python Crash Course

This repo contains some of the very first python projects I have ever worked on. 
The majority of the work here was made possible thanks to the author of this book:  
[Python Crash Course, 2nd Edition](https://nostarch.com/pythoncrashcourse2e).

## Resources

The resouces, as well as solutions, are under the _resources_ folder.  The author 
has also provided the book resources on GitHub.  Here is the link:  
https://github.com/ehmatthes/pcc_2e/