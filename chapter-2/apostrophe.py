# Using double and single quotes properly

message = "one of Python's strengths is its diverse community"
print(message)


# Using double quotes inside a string

famous_quote = "\nAlbert Einstein once said, “A person who never made a mistake never tried anything new.”"

print(famous_quote)

