# Changing the case in a String

name = "ada lovelace"

# Print variable using the title case method
print(name.title())

# Print the variable using the lowercase method
print(name.lower())

# Print the variable using the upppercase method
print(name.upper())

